// Dependencies
const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express');

const app = express();

// Certificate
const privateKey = fs.readFileSync('/etc/letsencrypt/live/yourdomain.com/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/yourdomain.com/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/yourdomain.com/chain.pem', 'utf8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};

app.use((req, res) => {
	res.send(`<!--need be in /var/www/errors -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>The site is undergoing technical work.</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="The site is undergoing technical work.">
        <style type="text/css">
            body {font-family: "Antic Slab", Arial, Helvetica, sans-serif;}
            .box {
                width: 800px;
                height:200px;
                position: absolute;
                top: 50%;
                left: 50%;
                background-color: #f8f8f8;
                border: 1px solid #e5e4e4;
                text-align: center;
                margin-top: -100px;
                margin-left: -400px;
            }
            h1 {
                color: #614444;
                font-size: 40px !important;
                font-weight: normal !important;
            }

        </style>
    <body>
        <div class="box">
            <h1>Technical break</h1>
            <p >Servicing the server. <br> Soon it will be cool here!<br> Community telegram channel: <a href='https://t.me/qzark1'>t.me/qzark1</a></p>
        </div>
    </body>
</html>
`);
});

// Starting both http & https servers
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

httpServer.listen(80, () => {
	console.log('HTTP Server running on port 80');
});

httpsServer.listen(433, () => {
	console.log('HTTPS Server running on port 443');
});
